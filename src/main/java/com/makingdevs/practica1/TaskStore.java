package com.makingdevs.practica1;

import java.util.List;

/**
 * Created by floobot on 13/06/17.
 */
public interface TaskStore {
    void createTask(Task task);
    Task readTask(Long pk);
    Task findTask(String description);
    List<Task> findAllTasks(String description);
    int count();
}
